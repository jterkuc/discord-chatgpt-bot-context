#Made with <3 by Jacob Terkuc

import os, discord, functions, asyncio, random, settings, schedule, character

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.cron import CronTrigger

from dotenv import load_dotenv

#grab tokens from .env
load_dotenv()
DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')

#Queues used to manage context memory
q_human = [""]
q_AI = [""]

intents = discord.Intents.default()
intents.message_content = True

discord_client = discord.Client(intents=intents)

#console message to indicate the bot has come online.
@discord_client.event
async def on_ready():
    print(f'{discord_client.user} has connected to Discord! (use keyword [' + settings.trigger_name + "] to trigger)")

#message event
@discord_client.event
async def on_message(message):

    global q_human
    global q_AI

    if message.author == discord_client.user:
        return

    if message.author.bot:
        return
    
    if character.trigger_name in message.content.lower():

        msg = message.content

        #Add user message to user queue
        functions.modifyQueue(q_human, msg)

        #Generate Response
        resp = functions.generate_response(functions.generatePrompt(q_human, q_AI))

        #Cleaning function (Deprecated)
        #print (resp)

        #splitText = resp.split("\n ")

        #respC = splitText[len(splitText) - 1]


        #Add AI response to queue
        functions.modifyQueue(q_AI, resp["choices"][0]["message"]["content"])

        #Debug
        #print(q_human)
        #print(q_AI)

        #Verify message is not null
        if(len(resp) > 1):
            print("New Message Generating:")

            await asyncio.sleep(random.randrange(settings.time_to_wait_min, settings.time_to_wait_max)) #wait 1 to 3 seconds before passing

            async with message.channel.typing():
                await asyncio.sleep(functions.generate_typetime(resp))        #send the typing indicator for 3 to 6 seconds

            #print("Pass to Discord API")                                      #status message send to console

            await message.channel.send(resp["choices"][0]["message"]["content"] + ' ')                           #send resp (generated message) to discord
            
            functions.debugPrint(resp["usage"]["total_tokens"], resp["choices"][0]["message"]["content"], resp["created"])

            return

        else:
            await message.channel.send(settings.default_response)

        

        


discord_client.run(DISCORD_TOKEN)

