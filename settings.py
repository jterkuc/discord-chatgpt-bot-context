#Made with <3 by Jacob Terkuc

import character

#Name that will trigger bot in discord
trigger_name=character.trigger_name

#----Character reference for bot to function properly----#
prompt = character.prompt
default_response = character.default_response

#------------------Text Queue Settings-------------------#
#Number of human and ai responses that will be queued. (Note: Higher values will consume more credits)
q_len = 3
#q_expire = 3000     #5 minutes (time in seconds) (TODO: IMPLIMENT)

#--------Sleep settings for bot typing indicator.--------#

#Min/max time before typing indicator (in seconds)
time_to_wait_min = 0
time_to_wait_max = 1

#Time multiplier bot will send typing indicator (in seconds)
#The calculation is done by taking the length of the message and dividing it by this value
t_speed_multiplier = 2

#-----------Configuration for the OpenAI Model-----------#
#Advanced users only: If you don't know what these do it's best to leave them.

model = "gpt-4"
#--------------------------------------------------------#
