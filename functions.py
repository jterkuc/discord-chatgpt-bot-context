#Made with <3 by Jacob Terkuc

import os, discord, functions, openai, random, settings, threading, character

from dotenv import load_dotenv

load_dotenv()
DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')
openai.api_key = os.getenv('OPENAI_API_KEY')

#Generates a response from openai API
def generate_response(genPrompt):

    #debug
    print(genPrompt)

    #Configuration for the openAI model.
    #Updated for new chat API, less settings required
    response = openai.ChatCompletion.create(
        model=settings.model,
        messages=genPrompt
    )

    #Return message content
    return response


def debugPrint(tokensUsed, message, time):

    #Open Logfile
    logFile = open("logfile.log", mode = "r+")

    #Debug terminal output + Write to logfile
    print("--- New Query ---\n" + "\nEpoch Time: " + str(time) + "\nUsage Stats: Tokens used: [" + str(tokensUsed) + "], Total Tokens Used this Session: [" + "]")
    print("Message: " + message)

    print("--- New Query ---\n" + "\nEpoch Time: " + str(time) + "\nUsage Stats: Tokens used: [" + str(tokensUsed) + "], Total Tokens Used this Session: [" + "]", file = logFile)
    print("Message: " + message, file = logFile)

    #Close Log
    logFile.close()
    return 0


#Determines how long the message typing indicator will be active
def generate_typetime(msg):

    #Formula: Length of message / speed multiplier defined in settings (Default: 20)
    return (len(msg)/settings.t_speed_multiplier)


#Adds a message to a Queue
def modifyQueue(q, message):

    #Check if length of queue is greater or equal to queue length value in settings (Default: 10)
    if(len(q) >= settings.q_len):
        q.pop(0)        #pop last message in queue

    q.append(message)   #Add new message
    return q


#Generates a prompt using the cache of AI and user input.
def generatePrompt(qPerson, qAI):
    
    #Create array
    prompt = []

    #Add prompt to array
    prompt.append({"role": "system", "content": character.prompt})

    #For each message in the qPerson array, generate a back and forth response ending with last AI response
    for x in range(len(qPerson) - 1):
        prompt.append({"role": "user", "content": qPerson[x]})
        prompt.append({"role": "assistant", "content": qAI[x]})

    #Insert last user response allowing AI to respond properly
    prompt.append({"role": "user", "content": qPerson[len(qPerson) - 1]})
    return prompt




#Build the prompt
#/def generatePrompt(qPerson, qAI, CName, CDescription):
#    prompt = (CDescription + "\n")
#    for x in range(len(qPerson) - 1):
#        prompt += "\n\nPerson: "
#        prompt += qPerson[x] 
#        prompt += ("\n\n" + CName + ": ")
#        prompt += (qAI[x-1])
#
#    prompt += "\n\nPerson: "
#    prompt += qPerson[(len(qPerson) - 1)]
#
#    prompt += ("\n\n" + CName + ": ")
#    
#    return prompt